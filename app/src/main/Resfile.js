var path = require('path');
var options = {
  // Define which platform the output for. android, ios, web
  // Leave only one of the following uncommented.
  //platform : 'web',
  //platform : 'ios',
  platform : 'android',
  //platform : 'redso-python',

  // Playroll all.csv
  spreadsheetKey: '1US_HVPm0bnnJA1wC6u62t8odvw6jw3UEOAWH5UCa0P8',
  spreadsheetGID: '0',

  // Define where the CSV file to load
  // The use of `path' module to handle cross platform issue.
  //csvPath : path.join('Users', 'edward', 'code', 'redso', 'playroll.web', 'asset', 'languages', 'all.csv'),
  csvPath : path.join(__dirname, 'strings.csv'),

  // Define where the result will be written for different platform. 
  // You can delete other platforms if you are going to use them.
  android: {
    dstPath: {
      // the key must match the csv header (1st row)
      'en':      path.join(__dirname, 'res', 'values',  'strings.xml'),
      'zh-Hant': path.join(__dirname, 'res', 'values-zh-rTW', 'strings.xml'),
      //'zh-Hans': path.join(__dirname, 'res', 'values-zh-rCN', 'strings.xml'),
    }
  },

  web: {
    dstPath: {
      'en':      path.join('./out', 'web', 'en.js'),
      'zh-Hant': path.join('./out', 'web', 'zh-hant.js'),
      'zh-Hans': path.join('./out', 'web', 'zh-hans.js'),
    }
  },

  ios: {
    dstPath: {
      // relative to the config.js
      'en':      path.join(__dirname, 'Res', 'en.sync.strings'),
      'zh-Hant': path.join(__dirname, 'Res', 'zh-Hant.sync.strings'),
      'zh-Hans': path.join(__dirname, 'Res', 'zh-Hans.sync.strings'),
      'header':  path.join(__dirname, 'Res', 'ResHeader.h'),
    }
  },

  'redso-python': {
    dstPath: {
      'en':      path.join('./out', 'redso_python', 'en.py'),
      'zh-Hant': path.join('./out', 'redso_python', 'zh-hant.py'),
      'zh-Hans': path.join('./out', 'redso_python', 'zh-hans.py'),
      'all': path.join('./out', 'redso_python', 'word_dict.py'),
    },
  },
};

exports.config = options;
