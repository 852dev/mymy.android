package com.mymy.feature.landing;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.mymy.feature.home.HomeActivity;
import com.mymy.R;
import com.mymy.data.DataManager;
import com.mymy.data.utils.SchedulerTransformer;

import java.util.HashMap;

public class LandindActivity extends AppCompatActivity {
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landind);

        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
        // If using in a fragment
        callbackManager = CallbackManager.Factory.create();


        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("callFacebookLoginApi", "callFacebookLoginApi" + loginResult.getAccessToken().getToken());
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("token", loginResult.getAccessToken().getToken());
                DataManager.getInstance()
                        .facebookLogin(map)
                        .compose(new SchedulerTransformer<>())
                        .subscribe(
                                user -> {
                                    goMain();
                                },
                                error -> {

                                }
                        );
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }


    public void goMain() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
