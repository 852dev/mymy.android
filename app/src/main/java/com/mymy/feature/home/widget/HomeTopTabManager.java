//package com.mymy.feature.home.widget;
//
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.view.PagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.mymy.app.BaseFragment;
//import com.ogaclejapan.smarttablayout.SmartTabLayout;
//import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
//import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
//
//public class HomeTopTabManager {
//
//  private SmartTabLayout tabLayout;
//  private ViewPager viewPager;
//  private FragmentPagerItemAdapter adapter;
//
//  private HomeTopTabManager() {
//
//  }
//
//  private enum Tab {
//    CUSTOMER(R.drawable.state_tv, PostsTvFragment.class),
//    SELLER(R.drawable.state_star, PostsFollowFragment.class);
//
//    int icon;
//    Class<? extends Fragment> fragment;
//    Bundle args;
//
//    Tab(int icon, Class<? extends Fragment> fragment, Bundle args) {
//      this.icon = icon;
//      this.fragment = fragment;
//      this.args = args;
//    }
//
//    Tab(int icon, Class<? extends Fragment> fragment) {
//      this(icon, fragment, null);
//    }
//  }
//
//  public static HomeTopTabManager init(final BaseFragment fragment) {
//    final HomeTopTabManager instance = new HomeTopTabManager();
//
//    FragmentPagerItems.Creator pages = FragmentPagerItems.with(fragment.getContext());
//    for (int i = 0; i < Tab.values().length; i++) {
//      Tab tab = Tab.values()[i];
//      if (tab.args != null) {
//        pages.add(tab.icon, tab.fragment, tab.args);
//      } else {
//        pages.add(tab.icon, tab.fragment);
//      }
//    }
//
//    instance.tabLayout = (SmartTabLayout) fragment.getView().findViewById(R.id.topTabLayout);
//    instance.viewPager = (ViewPager) fragment.getView().findViewById(R.id.topTabViewPager);
//    instance.adapter = new FragmentPagerItemAdapter(
//      fragment.getChildFragmentManager(), pages.create());
//    instance.viewPager.setAdapter(instance.adapter);
//    instance.tabLayout.setCustomTabView(new SmartTabLayout.TabProvider() {
//      @Override
//      public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
//        ImageView iconView = (ImageView) LayoutInflater.from(container.getContext()).inflate(R.layout.view_home_top_tab_item, container, false);
//        iconView.setImageResource(Tab.values()[position].icon);
//        return iconView;
//      }
//    });
//
//    instance.tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//      @Override
//      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
//
//      @Override
//      public void onPageSelected(int position) {
//
//      }
//
//      @Override
//      public void onPageScrollStateChanged(int state) {}
//    });
//
//    instance.tabLayout.setViewPager(instance.viewPager);
//
//    return instance;
//  }
//
//  public void changeTab(int pos) {
//    viewPager.setCurrentItem(pos);
//  }
//
//  public void reloadTab(int pos) {
//    Fragment fragment = adapter.getPage(pos);
//    if (fragment instanceof Reloadable) {
//      ((Reloadable) fragment).reload();
//    }
//  }
//
//  public void updateTabText(int pos, String text) {
//    ((TextView) tabLayout.getTabAt(pos)).setText(text);
//  }
//
//  public interface Reloadable {
//    void reload();
//  }
//
//}
