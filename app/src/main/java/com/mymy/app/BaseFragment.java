package com.mymy.app;

import android.widget.Toast;

import com.trello.rxlifecycle.components.support.RxFragment;


public class BaseFragment extends RxFragment {

//  public void showLoadingDialog() {
//    if (getActivity() != null) {
//      if (getActivity() instanceof BaseActivity) {
//        ((BaseActivity) getActivity()).showLoadingDialog();
//      }
//    }
//
//  }
//
//  public void hideLoadingDialog() {
//    if (getActivity() != null) {
//      if (getActivity() instanceof BaseActivity) {
//        ((BaseActivity) getActivity()).hideLoadingDialog();
//      }
//    }
//  }
//
//  public void showRefreshing(ColorfulSwipeRefreshLayout layout) {
//    layout.setRefreshing(true);
//  }
//
//  public void hideRefreshing(ColorfulSwipeRefreshLayout layout) {
//    layout.setRefreshing(false);
//  }
//
//  public void showMessageDialog(String text) {
//    if (getActivity() != null) {
//      if (getActivity() instanceof BaseActivity) {
//        ((BaseActivity) getActivity()).showMessageDialog(text);
//      }
//    }
//
//
//  }
//
//  public void showServerErrorDialog( ) {
//    if (getActivity() != null) {
//      if (getActivity() instanceof BaseActivity) {
//        ((BaseActivity) getActivity()).showServerErrorDialog( );
//      }
//    }
//
//
//  }
//
//
//  public void showMessageDialog(int text) {
//    if (getActivity() != null) {
//      if (getActivity() instanceof BaseActivity) {
//        ((BaseActivity) getActivity()).showMessageDialog(text);
//      }
//    }
//  }
//
//  public void showConnectSocialMessage(int text) {
//    if (getActivity() != null) {
//      if (getActivity() instanceof BaseActivity) {
//        ((BaseActivity) getActivity()).showConnectSocialMessage(text);
//      }
//    }
//  }
//
//  public void showNetworkErrorDialog() {
//    if (getActivity() != null) {
//      if (getActivity() instanceof BaseActivity) {
//        ((BaseActivity) getActivity()).showNetworkErrorDialog();
//      }
//    }
//  }
//
//  public void hideLoadingAndShowNetworkErrorDialog() {
//    if (getActivity() != null) {
//      if (getActivity() instanceof BaseActivity) {
//        ((BaseActivity) getActivity()).hideLoadingAndShowNetworkErrorDialog();
//      }
//    }
//  }
//
//  public void hideRefreshingAndShowNetworkErrorDialog(ColorfulSwipeRefreshLayout layout) {
//    hideRefreshing(layout);
//    showNetworkErrorDialog();
//  }

  public void showToast(int message) {
    Toast.makeText(App.getAppContext(), message, Toast.LENGTH_SHORT).show();
  }

  public void showToast(String message) {
    Toast.makeText(App.getAppContext(), message, Toast.LENGTH_SHORT).show();
  }


}

