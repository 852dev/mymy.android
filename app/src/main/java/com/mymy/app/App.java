package com.mymy.app;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.BuildConfig;
import android.support.multidex.MultiDexApplication;
import android.view.View;

import com.facebook.FacebookSdk;
import com.facebook.stetho.Stetho;
import com.orhanobut.logger.LogLevel;
import com.orhanobut.logger.Logger;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import java.util.List;

public class App extends MultiDexApplication {

  public static App me;

  @Override
  public void onCreate() {
    super.onCreate();
    me = this;

    Logger.init("logger")
      .logLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE)
      .methodCount(0).hideThreadInfo();

    FacebookSdk.sdkInitialize(getApplicationContext());


    if (BuildConfig.DEBUG) {
      Stetho.initialize(
        Stetho.newInitializerBuilder(this)
          .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
          .build());
    }
  }

  public static Context getAppContext() {
    return me.getApplicationContext();
  }

}
