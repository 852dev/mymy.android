package com.mymy.app;

import android.os.Bundle;
import com.afollestad.materialdialogs.MaterialDialog;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import javax.annotation.Nullable;

public class BaseActivity extends RxAppCompatActivity {

  private MaterialDialog loadingDialog;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
//    Fabric.with(this, new Crashlytics());

    if (savedInstanceState != null) {
      // restore global object here
    }
  }

}
