package com.mymy.data.utils;


import com.mymy.data.model.ApiResponse;

import rx.functions.Func1;

public class ApiResponseMapper<T> implements Func1<ApiResponse<T>, T> {

  @Override
  public T call(ApiResponse<T> serverResponse) {
    if (serverResponse.data == null) {
      throw new RuntimeException(serverResponse.returnCode + " : " + serverResponse.error_message);
    } else {
      saveMetaData(serverResponse);
      return serverResponse.data;
    }
  }

  private void saveMetaData(ApiResponse serverResponse) {

  }
}
