package com.mymy.data.remote;

import com.mymy.data.model.LoginResponse;

import java.util.HashMap;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface Api {

    @POST("/login/facebook")
    Observable<Void> facebookLogin(@Body HashMap<String, String> params);

}