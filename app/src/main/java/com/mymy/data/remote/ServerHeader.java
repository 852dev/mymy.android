package com.mymy.data.remote;
import java.util.HashMap;

import com.google.common.collect.Maps;

public class ServerHeader {

  public static HashMap<String, String> getDefaultHeaders() {
    HashMap<String, String> headers = new HashMap<>();
    headers.put("Content-Type", "application/json");
    return Maps.newHashMap(headers);
  }

}
