package com.mymy.data;

import android.net.Uri;
import android.text.TextUtils;


import com.mymy.data.model.LoginResponse;
import com.mymy.data.remote.Api;
import com.mymy.data.remote.ApiManager;
import com.mymy.data.utils.SchedulerTransformer;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import rx.Observable;

public class DataManager {

  private static final DataManager INSTANCE = new DataManager();
  private Api api;

  private DataManager() {
    this.api = ApiManager.getApi();
  }

  public static DataManager getInstance() {
    return INSTANCE;
  }

  public Observable<Void> facebookLogin(HashMap<String, String> map) {
    return api.facebookLogin(map)
      .compose(new SchedulerTransformer<>())
      ;
  }


}
