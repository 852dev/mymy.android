package com.mymy.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;

public class LoginResponse extends RealmObject implements Serializable {

  @SerializedName("token")
  public String token;
  @SerializedName("type")
  public String type;

  public LoginResponse() {

  }


}
