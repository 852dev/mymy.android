package com.mymy.data.model;

import com.google.gson.annotations.SerializedName;

public class ApiResponse<T> {
  @SerializedName(value = "postList", alternate = {
    "industries", "brandList"})
  public T data;
  @SerializedName("action")
  public String action;
  @SerializedName("returnCode")
  public String returnCode;
  @SerializedName("totalCount")
  public int totalCount;
  @SerializedName("error_message")
  public String error_message;
}
